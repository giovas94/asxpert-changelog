![AS Xpert](https://s3.amazonaws.com/as-public-images/ASxpert-logo.png)
# AS Xpert Changelog
Todos los cambios notables en este proyecto se documentarán en este archivo.

Seguimiento de cambios y : [Trello](https://trello.com/b/mD4KZH3V/asxpert)

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).


## [1.1.0] - 2018-08-07
### Added
- **alliance-client** @ obtener tipo de certificado (fiel o csd) automáticamente.
- **alliance-client** @ verificar RFC del certificado.
- 	**general** @ recordatorio por email al cliente a los 3 días si no ha enviado sus datos
- **general** @ recordatorio por email al cliente a los 7 días si no ha enviado sus datos
- **market** @ notificaciones vía email de nuevo mensaje
- **alliance-client** @ notificaciones vía email de nuevo mensaje
- **partner** @ notificaciones vía email de nuevo mensaje
- **admin** @ notificaciones vía email de nuevo mensaje
- **general** @ agregar productCode para facturación (Código del SAT)
- **client** @ agregar función de nueva contraseña para usuarios creados desde el **admin**
- **client** @ generación de factura CFDI de cada orden de servicio
- **client** @ el cliente puede agregar varios certificados y seleccionar el adecuado para cada servicio
- **admin** @ búsqueda de clientes por nombre, email y número de teléfono
- **admin** @ crear colaboradores
- **admin** @ crear clientes
- **admin** @ dashboard con gráficos y estadísticas de ventas


### Fixed
 - **partner** @ descarga de certificados de la orden asignada
 - **general** @ posibilidad de ver de quien es el mensaje recibido (cliente, colaborador o administrador)



![plukke.mx](https://res.cloudinary.com/dd7zufany/image/upload/v1532567772/plukke/plukke_transparent.png)